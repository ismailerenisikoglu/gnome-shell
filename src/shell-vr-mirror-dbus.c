#include "shell-vr-mirror-dbus.h"
#include "shell-vr-mirror.h"

#define BUS_NAME "org.gnome.Shell.XR"
static const gchar introspection_xml[] =
  "<node>"
  "  <interface name='org.gnome.Shell.XR'>"
  "    <property name='enabled' type='b' access='readwrite' />"
  "  </interface>"
  "</node>";

static void
_vrshell_set (gboolean enable)
{
  ShellVRMirror *vr_mirror = shell_vr_mirror_get_instance ();
  if (enable)
    {
      if (vr_mirror)
        {
          g_print ("DBus: Enable XR (already enabled)\n");
        }
      else
        {
          g_print ("DBus: Enable XR\n");
          shell_vr_mirror_create_instance ();
        }
    }
  else
    {
      g_print ("DBus: Disable XR\n");
      if (vr_mirror)
        {
          shell_vr_mirror_destroy_instance ();
        }
    }
}

static GVariant *
_handle_dbus_property_get (GDBusConnection *connection,
                           const gchar *sender,
                           const gchar *object_path,
                           const gchar *interface_name,
                           const gchar *property_name,
                           GError **error,
                           gpointer user_data)
{
  if (g_strcmp0 (property_name, "enabled") == 0)
    {
      ShellVRMirror *instance = shell_vr_mirror_get_instance ();
      gboolean vr_running = (instance != NULL);
      GVariant *ret = g_variant_new_boolean (vr_running);
      return ret;
    }
  else
    {
      g_print ("Error, only XR enabled property supported!\n");
      return NULL;
    }
}

static gboolean
_handle_dbus_property_set (GDBusConnection *connection,
                           const gchar *sender,
                           const gchar *object_path,
                           const gchar *interface_name,
                           const gchar *property_name,
                           GVariant *value,
                           GError **error,
                           gpointer user_data)
{
  if (g_strcmp0 (property_name, "enabled") == 0)
    {
      if (!g_variant_is_of_type (value, G_VARIANT_TYPE_BOOLEAN))
        {
          g_print ("Error, XR enabled property must be bool!\n");
          return FALSE;
        }
      gboolean setTo = g_variant_get_boolean (value);
      _vrshell_set (setTo);
      return TRUE;
    }
  return FALSE;
}

static const GDBusInterfaceVTable interface_vtable =
{
  NULL,
  _handle_dbus_property_get,
  _handle_dbus_property_set
};

static void
_on_dbus_bus_aquired (GDBusConnection *connection,
                      const gchar     *name,
                      gpointer         user_data)
{
  GDBusNodeInfo *introspection_data = g_dbus_node_info_new_for_xml (
    introspection_xml, NULL);

  g_dbus_connection_register_object (connection,
                                     "/org/gnome/Shell/XR",
                                     introspection_data->interfaces[0],
                                     &interface_vtable,
                                     NULL,  /* user_data */
                                     NULL,  /* user_data_free_func */
                                     NULL); /* GError** */
}

void
shell_vr_mirror_dbus_init (void)
{
  g_bus_own_name (G_BUS_TYPE_SESSION,
                  BUS_NAME,
                  G_BUS_NAME_OWNER_FLAGS_ALLOW_REPLACEMENT |
                  G_BUS_NAME_OWNER_FLAGS_REPLACE,
                  _on_dbus_bus_aquired,
                  NULL,
                  NULL,
                  NULL,
                  NULL);
}
